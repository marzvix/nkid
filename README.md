nkid - Jun/2024

My "AKID like" fullscreen editor 

   This project started from a testing BASIC routines for MSX screen handling.

   But soon I figured out that will easy convert my tests in a full-fledged fullscreen editor.

   I really don't like those AKID commands from AKID editor despite being a decent text editor for MSX.

   But my aim is to have a simple text editor like the unix ed to use in MSX-1 and MSX-2.

   I've even started a project "edsage" but I came across initial dificulties with RE and bufferization.

   So I start to consider start with full-fledged editor in BASIC and after that I pretend to restart edsage project.

  But when it good to run I will have two options: rebuild it in C or Assembly as new project optmizations.
  
  At end I consider developing an editor - whatever kind of editor - the best aproach to get acquainted with any computer environment. I realized it from Al Stevens D-Flat project that will influence this project or another yet project influenced by Dr Dobb's magazine (AYPIDDM)

   The name... 
   
   After a little thinking about I remember my childhood japanese Hero: National Kid.

   I'm using Panassonic FS-A1GT emulation and Panasonic - coincidence or not - is the rebranded name of National (Japanese Eletronic Company) and the former National Kid is the character for indirect merchandising on those 60's and 70's.

   By the way, the name "nkid" came from National Kid. (Awika!)
